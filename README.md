escooters-demand
==============================

ML-project for prediction of escooters demand in different districts of Chicago with emphasis on MLOps practices.

Project Organization
------------

    ├── LICENSE
    ├── Makefile           <- Makefile with commands like `make data` or `make train`
    ├── README.md          <- The top-level README for developers using this project.
    ├── data
    │   ├── external       <- Data from third party sources.
    │   ├── interim        <- Intermediate data that has been transformed.
    │   ├── processed      <- The final, canonical data sets for modeling.
    │   └── raw            <- The original, immutable data dump.
    │
    ├── docs               <- A default Sphinx project; see sphinx-doc.org for details
    │
    ├── models             <- Trained and serialized models, model predictions, or model summaries
    │
    ├── notebooks          <- Jupyter notebooks. Naming convention is a number (for ordering),
    │                         the creator's initials, and a short `-` delimited description, e.g.
    │                         `1.0-jqp-initial-data-exploration`.
    │
    ├── references         <- Data dictionaries, manuals, and all other explanatory materials.
    │
    ├── reports            <- Generated analysis as HTML, PDF, LaTeX, etc.
    │   └── figures        <- Generated graphics and figures to be used in reporting
    │
    ├── requirements.txt   <- The requirements file for reproducing the analysis environment, e.g.
    │                         generated with `pip freeze > requirements.txt`
    │
    ├── setup.py           <- makes project pip installable (pip install -e .) so src can be imported
    ├── src                <- Source code for use in this project.
    │   ├── __init__.py    <- Makes src a Python module
    │   │
    │   ├── data           <- Scripts to download or generate data
    │   │   └── make_dataset.py
    │   │
    │   ├── features       <- Scripts to turn raw data into features for modeling
    │   │   └── build_features.py
    │   │
    │   ├── models         <- Scripts to train models and then use trained models to make
    │   │   │                 predictions
    │   │   ├── predict_model.py
    │   │   └── train_model.py
    │   │
    │   └── visualization  <- Scripts to create exploratory and results oriented visualizations
    │       └── visualize.py
    │
    └── tox.ini            <- tox file with settings for running tox; see tox.readthedocs.io


--------

Прогнозирование востребованности электросамокатов в районах г.Чикаго
------------

Назначение проекта
--------

Компания предоставляет в аренду электросамокаты в 77 районах г.Чикаго. В каждом районе есть небольшой логистический центр - склад, в котором концентрируются электросамокаты, из которого ответственные за район сотрудники развозят самокаты в течение дня по разным локациям внутри района. Для того, чтобы обеспечить необходимое количество самокатов в районном складе, нужно иметь прогноз востребованности самокатов на ближайшие 3 дня. Такой горизонт прогнозирования обусловлен процессом логистического обеспечения районных складов электросамокатами из удаленных складов за городом, станций технического обслуживания электросамокатов и процессом закупки новых электросамокатов у поставщиков. Задача распределения электросамокатов внутри районов в течение дня решена. Целью Проекта является обеспечение Компании прогнозами востребованности электросамокатов в районах г.Чикаго для обеспечения этого спроса электросамокатами. Использование таких прогнозов позволит минимизировать простой самокатов (не держать излишки самокатов в районных складах) и минимизировать возникновение ситуаций нехватки самокатов в районных складах для удовлетворения спроса внутри района. В рамках Проекта будет разработано решение, которое позволит получать прогнозы количества поездок на электросамокатах Компании в каждом из 77 районов г.Чикаго. Прогнозирование будет выполняться на 3 следующих суток с гранулярностью 1 сутки для каждого района.

Цели проекта
--------

Реализовать и протестировать решения, основанные на ML-моделях, позволяющие прогнозировать востребованность электросамокатов в районах г.Чикаго Работоспособность решения подтверждена на наборе данных, который не использовался при разработке Решения; Качество моделей оценивается:

Экспертно;
Путем расчета метрик:
ошибка MAPE прогноза количества поездок по городу,
ошибка MAPE прогноза количества поездок по каждому району,
взвешенная на масштаб района ошибка APE прогноза количества поездок,
BCR(N) (bad case rate - доля районов с ошибкой MAPE прогноза количества поездок > N%),
отношение предсказанного количества поездок к фактическому (по районам отдельно и по городу в целом).
Метрики будет рассчитываться на основе прогноза на 3 следующих суток с гранулярностью 1 сутки для каждого района и для города в целом.

Задачи проекта
--------

В рамках проекта команде Проекта требуется выполнить следующие задачи:

Организация репозитория для хранения кода и артефактов Проекта;
Организация обеспечения качества исходного кода Решения;
Применение средств управления зависимостями Решения;
Выгрузка и обработка исторических данных о совершенных поездках на электросамокатах Компании;
Выгрузка и обработка географических данных, касающихся г.Чикаго;
Анализ выгруженных и обработанных данных;
Разработка и оценка модели(ей) прогнозирования востребованности электросамокатов Компании (количества поездок из района за сутки) по районам;
Разработка (настройка и применение) средств(а) автоматизации воспроизводимых масштабируемых исследований, проводимых в рамках Проекта;
Разработка сервиса на базе ML-модели(ей), предоставляющий прогнозы востребованности электросамокатов по районам, с применением контейнеризации (Docker);
Организация автоматизированного тестирования частей Решения;
Разработка средств мониторинга и аналитики работы сервиса и модели(ей);
Организация автоматизированного процесса развертывания разработанного Решения в продуктивной среде.
Результаты проекта

Результаты проекта должны включать в себя:
--------

Программный код Решения
Итоговый отчёт, включающий в себя:
Описание процесса сбора данных;
Результаты анализа и предобработки данных;
Метрики качества предсказаний модели;
Сравнение опробованных моделей/подходов и описание наилучшего с точки зрения метрик решения;
Описание границы применимости Решения;
Рекомендации по доработке Решения.
Ссылка на развернутое в сети Интернет Решений (сервис/приложение);
Презентация с выводами по итогу Проекта, рекомендациями и заключением по дальнейшему развитию проекта.