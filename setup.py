from setuptools import find_packages, setup

setup(
    name="src",
    packages=find_packages(),
    version="0.1.0",
    description="ML-project for prediction of escooters demand "
    "in different districts of Chicago with emphasis on MLOps practices.",
    author="Mops",
    license="",
)
