"""Module for generating features"""
import json
from itertools import product
from pathlib import Path
from typing import Tuple, Union, Dict, Any, List

import pandas as pd
from shapely.geometry import shape, Point

JSONType = Union[str, int, float, bool, None, Dict[str, Any], List[Any]]


def correct_formats(data: pd.DataFrame) -> pd.DataFrame:
    """
    Function corrects data formatting in rides data, such as Datetime, Float values etc.
    @param data: input pandas DataFrame
    @return: DataFrame with correct formats
    """
    data["Start Time"] = pd.to_datetime(data["Start Time"])
    data["End Time"] = pd.to_datetime(data["End Time"])
    data["Trip Distance"] = data["Trip Distance"].str.replace(",", ".").astype(float)
    data["Trip Duration"] = data["Trip Duration"].str.replace(",", ".").astype(float)
    return data


def build_features(data: pd.DataFrame) -> pd.DataFrame:
    """
    Function generates features based on input data
    @param data: input DataFrame
    @return: DataFrame with features
    """
    features_dataframe = (
        data.groupby(
            [pd.Grouper(key="Start Time", freq="d"), "Start Community Area Name"]
        )
        .agg(
            trip_count=("Start Community Area Name", "count"),
            total_distance=("Trip Distance", "sum"),
            mean_distance=("Trip Distance", "mean"),
            total_duration=("Trip Duration", "sum"),
            mean_duration=("Trip Duration", "mean"),
        )
        .reset_index()
    )
    return features_dataframe


def get_dataset_to_featurize(rides_df: pd.DataFrame) -> pd.DataFrame:
    """
    Function creates a dataset with all combinations of communities and dates
    @param rides_df: pd.DataFrame, input DataFrame with rides data
    @return: pd.DataFrame, a dataset with all combinations of communities
    and dates and target value for each combination  - the number of rides
    """
    rides_df["start_day"] = rides_df["Start Time"].dt.date
    days = list(
        pd.date_range(
            rides_df["start_day"].min(), rides_df["start_day"].max(), freq="1D"
        )
    )

    full_df = pd.DataFrame(
        list(product(days, rides_df["Start Community Area Name"].unique())),
        columns=["start_day", "community"],
    )
    full_df.sort_values(["start_day", "community"], inplace=True)
    full_df["community"] = full_df["community"].astype(str)

    community_rides_stat = (
        rides_df.groupby(
            [pd.Grouper(key="Start Time", freq="d"), "Start Community Area Name"]
        )
        .agg(rides_number=("Start Community Area Name", "count"))
        .reset_index()
    )
    community_rides_stat["start_day"] = pd.to_datetime(
        community_rides_stat["Start Time"].dt.date
    )
    community_rides_stat.columns = [
        "Start Time",
        "community",
        "rides_number",
        "start_day",
    ]

    full_df = full_df.merge(
        community_rides_stat, how="left", on=["start_day", "community"]
    ).fillna(0)
    full_df = full_df[["start_day", "community", "rides_number"]]

    return full_df


def build_features_on_date(data: pd.DataFrame) -> pd.DataFrame:
    """
    Function generates features based on date
    @param data: input DataFrame
    @return: DataFrame with features on date
    """

    data["day_of_year"] = data["start_day"].dt.dayofyear
    data["day_of_week"] = data["start_day"].dt.day_of_week
    data["is_weekend"] = data["day_of_week"].isin({5, 6}).astype(int)
    data["week"] = data["start_day"].dt.isocalendar().week

    return data


def get_center_lat_lon(path_to_file: Path) -> Tuple[float, float]:
    """
    Function loads the coordinates of the city center from a file
    @param path_to_file:
    @return:
    """

    with open(path_to_file, "r", encoding="utf-8") as file_with_coordinates:
        city_center_coordinates = file_with_coordinates.read()
        lat, lon = [float(x) for x in city_center_coordinates.split(",")]

    return lat, lon


def build_features_on_geodata(
    data: pd.DataFrame, boundaries: JSONType, lat: float, lon: float
) -> pd.DataFrame:
    """
    Function generates features based on geographical data -
    communities geometry and coordinates of the city center
    @param data: input DataFrame
    @param boundaries: geometry of communities boundaries
    @param lat: latitude of the city center
    @param lon: longitude of the city center
    @return: DataFrame with features on geographical data
    """

    communities_geometry_dict = {
        feature["properties"]["community"]: feature["geometry"]
        for feature in boundaries
    }

    data = data[data["community"] != "None"]
    data["geometry"] = data["community"].apply(
        lambda x: shape(communities_geometry_dict[x])
    )
    data["area"] = data["geometry"].apply(lambda x: x.area)
    data["distance_to_center"] = data["geometry"].apply(
        lambda x: x.centroid.distance(Point(lon, lat))
    )

    return data


if __name__ == "__main__":

    project_dir = Path(__file__).resolve().parents[2]
    rides_data_filepath = Path.joinpath(project_dir, "data/raw/rides_data.parquet")
    input_data = pd.read_parquet(rides_data_filepath)

    clean_data = correct_formats(input_data)

    dataset_to_featurize = get_dataset_to_featurize(clean_data)
    features = build_features_on_date(dataset_to_featurize)

    with open(
        Path.joinpath(project_dir, "data/raw/boundaries.json"), "r", encoding="utf-8"
    ) as f:
        geometry_data = json.load(f)["features"]

    latitude, longitude = get_center_lat_lon(
        Path.joinpath(project_dir, "data/external/city_center_coordinates.txt")
    )

    features = build_features_on_geodata(features, geometry_data, latitude, longitude)

    features_filepath = Path.joinpath(
        project_dir, "data/interim/interim_features.parquet"
    )
    features_filepath.parent.mkdir(parents=True, exist_ok=True)

    features_names = [
        "start_day",
        "community",
        "rides_number",
        "day_of_year",
        "day_of_week",
        "is_weekend",
        "week",
        "area",
        "distance_to_center",
    ]
    features[features_names].to_parquet(features_filepath)
