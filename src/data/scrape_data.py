"""Module providing functions for scraping external data"""
import logging
from pathlib import Path
from typing import Tuple
import requests
from bs4 import BeautifulSoup

COORDINATES_PAGE_URL = "https://www.latlong.net/place/chicago-il-usa-1855.html"
LOG_FORMAT = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"


def scrape_city_center_coordinates(
    url: str, city_center_filepath: Path
) -> Tuple[float, float]:
    """
    Scrapes data on city center coordinates and saves it to a text file
    @param url: str, the address of the page to scrape
    the coordinates of the city center from
    @param city_center_filepath: Path, the path of a file
    to save the city center coordinates into
    @return: Tuple[float, float], the coordinates of the city center
    """

    if city_center_coordinates_filepath.exists():
        with open(city_center_filepath, "r", encoding="utf-8") as file_with_coords:
            coordinates_str_list = file_with_coords.read().split(",")
            lat, lon = float(coordinates_str_list[0]), float(coordinates_str_list[1])
            print("skipping scrapping")
    else:
        html_doc = requests.get(url, allow_redirects=True, timeout=3).content
        soup = BeautifulSoup(html_doc, "html.parser")
        coordinates_element = soup.find_all(
            lambda tag: len(tag.find_all("p")) == 0
            and "Latitude and longitude coordinates are:" in tag.text
        )[0]
        coordinates_str = coordinates_element.find("strong").text
        city_center_coordinates_filepath.parent.mkdir(parents=True, exist_ok=True)
        with open(
            city_center_coordinates_filepath, "w", encoding="utf-8"
        ) as file_with_coords:
            file_with_coords.write(coordinates_str)
        print(coordinates_str)
        coordinates_str_list = coordinates_str.split(",")
        lat, lon = float(coordinates_str_list[0]), float(coordinates_str_list[1])
        print("scrapping finished")

    return lat, lon


if __name__ == "__main__":

    logging.basicConfig(level=logging.INFO, format=LOG_FORMAT)

    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    city_center_coordinates_filepath = Path.joinpath(
        project_dir, "data/external/city_center_coordinates.txt"
    )
    latitude, longitude = scrape_city_center_coordinates(
        COORDINATES_PAGE_URL, city_center_coordinates_filepath
    )

    print(latitude, longitude)
